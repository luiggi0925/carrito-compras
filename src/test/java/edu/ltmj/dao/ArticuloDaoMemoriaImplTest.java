package edu.ltmj.dao;

import java.math.BigDecimal;
import java.util.List;

import org.hamcrest.Matchers;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import edu.ltmj.entidad.Articulo;

public class ArticuloDaoMemoriaImplTest {

	private ArticuloDaoMemoriaImpl daoMemoriaImpl;

	@Before
	public void init() {
		daoMemoriaImpl = new ArticuloDaoMemoriaImpl();		
		
	}

	@Test
	public void create() {
		Articulo articulo = new Articulo();
		daoMemoriaImpl.create(articulo);
		Assert.assertThat(articulo.getId(), Matchers.equalTo(1));
	}

	@Test
	public void createTwoElements() {
		Articulo articulo = new Articulo();
		Articulo articulo2 = new Articulo();
		daoMemoriaImpl.create(articulo);
		daoMemoriaImpl.create(articulo2);
		Assert.assertThat(articulo.getId(), Matchers.equalTo(1));
		Assert.assertThat(articulo2.getId(), Matchers.equalTo(2));
	}

	@Test
	public void readConId() {
		Articulo articulo = new Articulo();
		daoMemoriaImpl.create(articulo);

		Articulo articuloIngresado = daoMemoriaImpl.read(1);
		Assert.assertThat(articuloIngresado, Matchers.not(Matchers.nullValue()));
		Assert.assertThat(articuloIngresado.getId(), Matchers.greaterThan(0));
		Assert.assertThat(articuloIngresado, Matchers.equalTo(articulo));
	}

	@Test
	public void readConIdNoExistente() {
		Articulo articuloIngresado = daoMemoriaImpl.read(1);
		Assert.assertThat(articuloIngresado, Matchers.not(Matchers.nullValue()));
		Assert.assertThat(articuloIngresado.getId(), Matchers.equalTo(0));
	}

	@Test
	public void readTodos() {
		Articulo articulo1 = new Articulo();
		articulo1.setNombre("prueba1");
		articulo1.setPrecioUnidad(new BigDecimal("1.0"));
		
		Articulo articulo2 = new Articulo();
		articulo2.setNombre("prueba2");
		articulo2.setPrecioUnidad(new BigDecimal("1.5"));
		daoMemoriaImpl.create(articulo1);
		daoMemoriaImpl.create(articulo2);

		List<Articulo> articuloList = daoMemoriaImpl.read();
		Assert.assertThat(articuloList, Matchers.not(Matchers.nullValue()));
		Assert.assertThat(articuloList, Matchers.is(Matchers.not(Matchers.empty())));
		Assert.assertThat(articuloList.size(), Matchers.equalTo(2));
	}

	@Test
	public void update() {
		Articulo articulo1 = new Articulo();
		articulo1.setNombre("prueba1");
		articulo1.setPrecioUnidad(new BigDecimal("1.0"));
		daoMemoriaImpl.update(articulo1);
	}
	
	@Test
	public void delete() {
		Articulo articulo1 = new Articulo();
		articulo1.setNombre("prueba1");
		articulo1.setPrecioUnidad(new BigDecimal("1.0"));
		daoMemoriaImpl.delete(articulo1);
	}
	
	@Test
	public void existsFalse() {
		Articulo articulo1 = new Articulo();
		articulo1.setNombre("prueba1");
		articulo1.setId(500);
		articulo1.setPrecioUnidad(new BigDecimal("1.0"));
		daoMemoriaImpl.exists(articulo1.getId());
	}
	
	@Test
	public void existsTrue() {
		Articulo articulo1 = new Articulo();
		articulo1.setNombre("prueba1");
		articulo1.setId(500);
		articulo1.setPrecioUnidad(new BigDecimal("1.0"));
		daoMemoriaImpl.create(articulo1);
		daoMemoriaImpl.exists(articulo1.getId());
	}
	

	
	//TAREA: crear tests para update, delete y exist
}
