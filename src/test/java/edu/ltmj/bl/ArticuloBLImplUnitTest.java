package edu.ltmj.bl;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import org.hamcrest.Matchers;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.mockito.Mockito;

import edu.ltmj.dao.ArticuloDao;
import edu.ltmj.entidad.Articulo;

public class ArticuloBLImplUnitTest {

	ArticuloBLImpl articuloBLImpl;
	
	@Rule public ExpectedException exception = ExpectedException.none();
	
	@Before
	public void init() {
		articuloBLImpl = new ArticuloBLImpl();
		
		ArticuloDao articuloDao = Mockito.mock(ArticuloDao.class);

		Articulo articulo = new Articulo();
		articulo.setId(1);
		articulo.setNombre("prueba");
		articulo.setPrecioUnidad(new BigDecimal("1.0"));
		Mockito.doReturn(articulo).when(articuloDao).read(articulo.getId());
		Mockito.doReturn(true).when(articuloDao).exists(articulo.getId());

		articuloBLImpl.setArticuloDao(articuloDao);
	}
	
	@Test
	public void getDao() {
		articuloBLImpl.getArticuloDao();
	}

	@Test
	public void createArticuloVacio() {
		exception.expect(IllegalArgumentException.class);
		exception.expectMessage("El artículo no debe ser nulo.");
		articuloBLImpl.create(null);
	}
	
	@Test
	public void createArticuloNombreNull() {
		exception.expect(IllegalArgumentException.class);
		exception.expectMessage("El nombre del artículo no puede ser nulo.");
		Articulo articulo = new Articulo();
		articulo.setId(2);
		articulo.setPrecioUnidad(new BigDecimal(2.0));
		articuloBLImpl.create(articulo);
	}

	@Test
	public void createNombreConEspacios() {
		String nombre = "   prueba   ";
		Articulo articulo = new Articulo();
		articulo.setNombre(nombre);
		articulo.setPrecioUnidad(new BigDecimal("1.0"));
		articuloBLImpl.create(articulo);
		Assert.assertThat(articulo.getNombre(), Matchers.equalTo(nombre.trim()));
	}

	@Test
	public void createNombreVacio() {
		exception.expect(IllegalArgumentException.class);
		exception.expectMessage("El nombre del artículo no puede estar vacío.");
		Articulo articulo = new Articulo(); 
		articulo.setNombre("");
		articulo.setId(1);
		articulo.setPrecioUnidad(new BigDecimal(2));
		articuloBLImpl.create(articulo);
	}
	
	@Test
	public void createPrecioUnidadNull() {
		exception.expect(IllegalArgumentException.class);
		exception.expectMessage("El precio del artículo no puede ser nulo.");
		Articulo articulo = new Articulo(); 
		articulo.setNombre("ASD");
		articulo.setId(1);
		articulo.setPrecioUnidad(null);
		articuloBLImpl.create(articulo);
	}
	
	@Test
	public void createPrecioMenorACero() {
		exception.expect(IllegalArgumentException.class);
		exception.expectMessage("El precio del artículo debe ser mayor a 0.");
		Articulo articulo = new Articulo(); 
		articulo.setNombre("Mateo");
		articulo.setId(1);
		articulo.setPrecioUnidad(new BigDecimal(-1));
		articuloBLImpl.create(articulo);
	}
	
	
	@Test
	public void create() {
		String nombre = "prueba";
		Articulo articulo = new Articulo();
		articulo.setNombre(nombre);
		articulo.setPrecioUnidad(new BigDecimal("1.0"));
		articuloBLImpl.create(articulo);
		Assert.assertThat(articulo.getNombre(), Matchers.equalTo(nombre));
	}

	

	
	
	
	
	@Test
	public void readPorId() {
		int id = 1;
		Articulo articulo = articuloBLImpl.read(id);
		Assert.assertThat(articulo, Matchers.not(Matchers.nullValue()));
		Assert.assertThat(articulo.getId(), Matchers.equalTo(id));
	}

	@Test
	public void read() {
		int id = 1;
		Articulo art = articuloBLImpl.read(id);
		Assert.assertThat(art.getId(), Matchers.equalTo(id));
	}
	
	
	@Test
	public void updateNull() {
		exception.expect(IllegalArgumentException.class);
		exception.expectMessage("El artículo no debe ser null");
		articuloBLImpl.update(null);
	}
	
	@Test
	public void updateDontExist() {
		exception.expect(IllegalArgumentException.class);
		exception.expectMessage("El artículo no existe");
		Articulo articulo = new Articulo();
		articulo.setId(2);
		articuloBLImpl.update(articulo);
	}
	
	@Test
	public void updatePrecioNull() {
		exception.expect(IllegalArgumentException.class);
		exception.expectMessage("El precio del artículo debe estar completo");
		Articulo articulo = new Articulo();
		articulo.setId(1);
		articulo.setPrecioUnidad(null);
		articuloBLImpl.update(articulo);
	}
	

	
	/*@Test
	public void updateIdMenorACero() {
		exception.expect(IllegalArgumentException.class);
		exception.expectMessage("El artículo buscado no existe.(ID>=0)");
		Articulo articulo = new Articulo();
		articulo.setId(0);
		articulo.setNombre("Mateo");
		articuloBLImpl.update(articulo);
	}*/

	@Test
	public void updatePrecioMenorACero() {
		exception.expect(IllegalArgumentException.class);
		exception.expectMessage("El precio debe ser mayor a 0");
		Articulo articulo = new Articulo(); 
		articulo.setId(1);
		articulo.setPrecioUnidad(new BigDecimal(-1));
		articuloBLImpl.update(articulo);
	}
	
	@Test
	public void updateNombreNull() {
		exception.expect(IllegalArgumentException.class);
		exception.expectMessage("El nombre no debe ser null");
		Articulo articulo = new Articulo(); 
		articulo.setId(1);
		articulo.setPrecioUnidad(new BigDecimal(2));
		articuloBLImpl.update(articulo);
	}
	
	@Test
	public void updateNombreIsEmpty() {
		exception.expect(IllegalArgumentException.class);
		exception.expectMessage("El nombre del artículo debe estar completo");
		Articulo articulo = new Articulo(); 
		articulo.setId(1);
		articulo.setPrecioUnidad(new BigDecimal(2));
		articulo.setNombre("");
		articuloBLImpl.update(articulo);
	}
	
	@Test
	public void updateNombreIsNotEmpty() {
		Articulo articulo = new Articulo(); 
		articulo.setId(1);
		articulo.setPrecioUnidad(new BigDecimal(2));
		articulo.setNombre("asd");
		articuloBLImpl.update(articulo);
	}
	
	@Test
	public void remove() {
		Articulo articulo = new Articulo();
		articulo.setId(2);
		articuloBLImpl.remove(articulo);
	}
	
	@Test
	public void readAll() {
		articuloBLImpl.read();
	}
	
	@Test
	public void exist() {
		articuloBLImpl.exists(1);
	}
	
	@Test
	public void existFalse() {
		articuloBLImpl.exists(4);
	}
	

}
