package edu.ltmj.vista.consola;

import java.math.BigDecimal;
import java.util.List;
import java.util.Scanner;

import edu.ltmj.bl.ArticuloBL;
import edu.ltmj.bl.factory.ConfiguracionAplicacion;
import edu.ltmj.entidad.Articulo;

public class ConsolaArticulo {

	private Scanner scanner;
	private ArticuloBL articuloBL;
	private boolean vivo = true;

	private void setScanner(Scanner scanner) {
		this.scanner = scanner;
	}

	private void setArticuloBL(ArticuloBL articuloBL) {
		this.articuloBL = articuloBL;
	}

	private void creaArticulo() {
		System.out.print("Ingrese el nombre del artículo: ");
		String nombre = scanner.nextLine();
		System.out.print("Ingrese el precio del artículo: ");
		BigDecimal precioUnidad = new BigDecimal(scanner.nextLine());
		Articulo articulo = new Articulo();
		articulo.setNombre(nombre);
		articulo.setPrecioUnidad(precioUnidad);
		articuloBL.create(articulo);
		System.out.println("El artículo fue registrado exitosamente.");
	}

	private void listarArticulos() {
		System.out.println("Se muestran los articulos");
		List<Articulo> listaArticulo = articuloBL.read();
		System.out.println("id\tnombre");
		System.out.println("----------------------------------------");
		for (Articulo articulo : listaArticulo) {
			System.out.println(String.format("%d\t%s", articulo.getId(), articulo.getNombre()));
		}
	}

	private void verDetalleArticulo() {
		System.out.print("Ingrese id del articulo: ");
		int id = scanner.nextInt();
		scanner.nextLine();
		Articulo articulo = articuloBL.read(id);
		if (articulo.getId() == 0) {
			System.out.println("No existe el artículo solicitado");
		} else {
			System.out.println("Detalles del artículo:");
			System.out.println("Nombre: " + articulo.getNombre());
			System.out.println("Precio por unidad: " + articulo.getPrecioUnidad());
		}
	}

	private void actualizarArticulo() {
		System.out.println("Ingrese el id del artículo a actualizar: ");
		int id = scanner.nextInt();
		scanner.nextLine();
		// FIXME: ¿Qué sucede si el artículo no existe? Debería mostrar un
		// mensaje de error
		// de que el artículo no se puede actualizar.
		if (articuloBL.exists(id)) {
			Articulo articulo = new Articulo();
			articulo.setId(id);
			System.out.println("Nombre del artículo: ");
			articulo.setNombre(scanner.nextLine());
			System.out.println("Precio del artículo: ");
			articulo.setPrecioUnidad(scanner.nextBigDecimal());
			try {
				articuloBL.update(articulo);
			} catch (IllegalArgumentException e) {
				System.out.println("Error en el artículo");
			}
		} else {
			System.out.println("El artículo no existe.");
		}


	}

	private void eliminarArticulo() {
		System.out.println("Ingrese el id del artículo a eliminar: ");
		int id = scanner.nextInt();
		scanner.nextLine();
		if (articuloBL.exists(id)) {
			Articulo articulo = new Articulo();
			articulo.setId(id);
			articuloBL.remove(articulo);
			System.out.println("El Artículo ha sido eliminado con exito.");
		} else {
			System.out.println("El artículo a eliminar no existe.");
		}
	}

	private void exists() {
		System.out.println("Ingrese el id del artículo a buscar: ");
		int id = scanner.nextInt();
		if (articuloBL.exists(id)) {
			System.out.println("El artículo existe.");
		} else {
			System.out.println("El artículo no existe.");
		}
	}

	private void despedida() {
		System.out.println("Gracias por utilizar el sistema. Bye!");
		System.exit(0);
		vivo = false;
	}

	private void muestraOpciones() {
		System.out.println("BIENVENIDO AL SISTEMA DE REGRISTRO DE ARTICULOS");
		while (vivo) {
			try {
				System.out.println("-----------------------------------------------");
				System.out.println("Opciones del sistema");
				System.out.println("1. Crear articulo.");
				System.out.println("2. Listar articulos.");
				System.out.println("3. Ver detalles de articulo.");
				System.out.println("4. Actualizar artículo.");
				System.out.println("5. Eliminar artículo.");
				System.out.println("6. Existe el artículo?.");
				System.out.println("7. Salir.");
				System.out.println("-----------------------------------------------");
				System.out.print("Por favor seleccione opción: ");
				validaOpcion();
			} catch (Exception e) {
				System.out.println("Problema en la aplicación: " + e.getMessage());
				// FIXME por tiempo de desarrollo, debería manejarse con un
				// Logger
				e.printStackTrace(System.out);
			}
		}
	}

	private void validaOpcion() {
		String opcion = scanner.nextLine();
		switch (opcion) {
		case "1":
			creaArticulo();
			break;
		case "2":
			listarArticulos();
			break;
		case "3":
			verDetalleArticulo();
			break;
		case "4":
			actualizarArticulo();
			break;
		case "5":
			eliminarArticulo();
			break;
		case "6":
			exists();
			break;
		case "7":
			despedida();
		default:
			System.out.println("Opción inválida. Vuelva a intentar.");
		}
		scanner.nextLine();
	}

	public static void main(String[] args) {
		ConsolaArticulo consola = new ConsolaArticulo();
		ConfiguracionAplicacion configApp = new ConfiguracionAplicacion();
		configApp.init();
		if (args != null && args.length	> 0) {
			if (args[0] != null && !"".equals(args[0].trim())
					&& "memoria".equals(args[0].trim().toLowerCase())) {
				configApp.configuracionMemoria();
			} else {
				configApp.configuracionBaseDatos();
			}
		} else {
			configApp.configuracionBaseDatos(); //Configuración default.
		}
		try (Scanner scanner = new Scanner(System.in)) {
			consola.setScanner(scanner);
			consola.setArticuloBL(configApp.getInstance(ArticuloBL.class));
			consola.muestraOpciones();
		}
	}
}
