package edu.ltmj.dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import edu.ltmj.entidad.Articulo;

public class ArticuloDaoJdbcImpl implements ArticuloDao {

	ConeccionSql coneccionSql;
	
	public ArticuloDaoJdbcImpl() {
		

	}

//	private int obtenerLlaveGenerada(PreparedStatement pstmt) throws SQLException {
//		int idGenerado = 0;
//		try (ResultSet rs = pstmt.getGeneratedKeys()) {
//			if (rs != null) {
//				if (rs.next()) {
//					idGenerado = rs.getInt(1);
//				}
//			}
//		}
//		return idGenerado;
//	}

	@Override
	public void create(Articulo articulo) {
		try (Connection con = coneccionSql.obtenerConexion()) {
			String insertSQL = "INSERT INTO articulo (nombre, precioUnidad) VALUES (?,?)";
			try (PreparedStatement pstmt = con.prepareStatement(insertSQL)) {
				pstmt.setString(1, articulo.getNombre());
				pstmt.setBigDecimal(2, articulo.getPrecioUnidad());
				pstmt.executeUpdate();
				//articulo.setId(obtenerLlaveGenerada(pstmt)); 
			}
		} catch (SQLException e) {
			throw new RuntimeException("Problema de SQL.", e);
		}
	}

	@Override
	public Articulo read(int id) {
		Articulo articulo = new Articulo();
		try (Connection con = coneccionSql.obtenerConexion()) {
			String selectSQL = "SELECT nombre, precioUnidad FROM articulo WHERE id = ?";
			try (PreparedStatement pstmt = con.prepareStatement(selectSQL)) {
				pstmt.setInt(1, id);
				try (ResultSet rs = pstmt.executeQuery()) {
					if (rs.next()) {
						articulo.setId(id);
						articulo.setNombre(rs.getString("nombre"));
						articulo.setPrecioUnidad(rs.getBigDecimal("precioUnidad"));
					}
				}
			}
		} catch (SQLException e) {
			throw new RuntimeException("Problema de SQL.", e);
		}
		return articulo;
	}

	@Override
	public List<Articulo> read() {
		List<Articulo> listaArticulos = new ArrayList<>();
		try (Connection con = coneccionSql.obtenerConexion()) {
			String selectSQL = "SELECT id, nombre FROM articulo";
			try (PreparedStatement pstmt = con.prepareStatement(selectSQL)) {
				try (ResultSet rs = pstmt.executeQuery()) {
					while (rs.next()) {
						Articulo articulo = new Articulo();
						articulo.setId(rs.getInt("id"));
						articulo.setNombre(rs.getString("nombre"));
						//articulo.setPrecioUnidad(rs.getBigDecimal("precioUnidad"));
						listaArticulos.add(articulo);
					}
				}
			}
		} catch (SQLException e) {
			throw new RuntimeException("Problema de SQL.", e);
		}
		return listaArticulos;
	}

	@Override
	public void update(Articulo articulo) {
		try (Connection con = coneccionSql.obtenerConexion()) {
			String updateSQL = "UPDATE articulo SET NOMBRE = ? , precioUnidad = ? WHERE id = ?";
			try (PreparedStatement pstmt = con.prepareStatement(updateSQL)) {
				pstmt.setString(1, articulo.getNombre());
				pstmt.setBigDecimal(2,articulo.getPrecioUnidad());
				pstmt.setInt(3,articulo.getId());
				pstmt.executeUpdate();
				//articulo.setId(obtenerLlaveGenerada(pstmt));
			}
		} catch (SQLException e) {
			throw new RuntimeException("Problema de SQL.", e);
		}
	}

	@Override
	public void delete(Articulo articulo) {
		try (Connection con = coneccionSql.obtenerConexion()) {
			String deleteSQL = "DELETE FROM articulo WHERE id = ?";
			try (PreparedStatement pstmt = con.prepareStatement(deleteSQL)) {
				pstmt.setInt(1,articulo.getId());
				pstmt.executeUpdate();
			}
		} catch (SQLException e) {
			throw new RuntimeException("Problema de SQL.", e);
		}
	}

	@Override
	public boolean exists(int id) {
//		if (read(id).getNombre() != null ) {//Quiere decir, que en el read no se encontró el artículo por lo que sus atributos serán null.
//			return true;
//		}
//		return false;
		boolean result = false;
		try (Connection con = coneccionSql.obtenerConexion()) {
			String existsSQL = "SELECT COUNT(1) AS totalRows FROM articulo WHERE id = ?";
			try (PreparedStatement pstmt = con.prepareStatement(existsSQL)) {
				pstmt.setInt(1,id);
				try (ResultSet rs = pstmt.executeQuery()) {
					if (rs.next()) {
						result = rs.getLong("totalRows") > 0;
					}
				}
			}
		} catch (SQLException e) {
			throw new RuntimeException("Problema de SQL.", e);
		}
		return result;
	}

	
	
	
	public ConeccionSql getConeccionSql() {
		return coneccionSql;
	}
	public void setConeccionSql(ConeccionSql coneccionSql) {
		this.coneccionSql = coneccionSql;
	}
	
	
	
	
	
	
	

}
