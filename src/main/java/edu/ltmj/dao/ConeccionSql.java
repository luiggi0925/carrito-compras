package edu.ltmj.dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class ConeccionSql {

	public Connection obtenerConexion() throws SQLException {
		String url = "jdbc:mysql://localhost:8080/carrito_compras";
		String user = "root";
		String password = "root";
		return DriverManager.getConnection(url, user, password);
	}
	
}
