package edu.ltmj.dao;

import java.util.List;

import edu.ltmj.entidad.Articulo;

public interface ArticuloDao {

	void create(Articulo articulo);
	Articulo read(int id);
	List<Articulo> read();
	void update(Articulo articulo);
	void delete(Articulo articulo);

	boolean exists(int id);
}
