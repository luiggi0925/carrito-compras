package edu.ltmj.dao;

import edu.ltmj.entidad.Articulo;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by MatiEzelQ on 1/5/16.
 */
public class ArticuloDaoMemoriaImpl implements ArticuloDao {

    Map<Integer,Articulo> mapa = new HashMap<>();

    @Override
    public void create(Articulo articulo) {
        articulo.setId(mapa.size()+1);
        mapa.put(articulo.getId(),articulo);
    }

    @Override
    public Articulo read(int id) {
        Articulo articulo = mapa.get(id);
        if (articulo == null) {
            articulo = new Articulo();
        }
        return articulo;
    }

    @Override
    public List<Articulo> read() {
        return new ArrayList<Articulo>(mapa.values());
    }

    @Override
    public void update(Articulo articulo) {
        mapa.put(articulo.getId(),articulo);
    }

    @Override
    public void delete(Articulo articulo) {
        mapa.remove(articulo.getId());
    }

    @Override
    public boolean exists(int id) {
        return (mapa.get(id) != null) ? true : false;
    }
}
