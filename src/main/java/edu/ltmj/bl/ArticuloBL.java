package edu.ltmj.bl;

import java.util.List;

import edu.ltmj.entidad.Articulo;

public interface ArticuloBL {

	void create(Articulo articulo);
	Articulo read(int id);
	void update(Articulo articulo);
	List<Articulo> read();
	void remove(Articulo articulo);
	boolean exists(int id);
}
