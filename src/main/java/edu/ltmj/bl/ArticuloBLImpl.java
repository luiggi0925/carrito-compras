package edu.ltmj.bl;

import java.math.BigDecimal;
import java.util.List;

import edu.ltmj.dao.ArticuloDao;
import edu.ltmj.entidad.Articulo;

public class ArticuloBLImpl implements ArticuloBL {

	ArticuloDao articuloDao;

	public ArticuloDao getArticuloDao() {
		return articuloDao;
	}

	public void setArticuloDao(ArticuloDao articuloDao) {
		this.articuloDao = articuloDao;
	}

	@Override
	public void create(Articulo articulo) {
		if (articulo == null) {
			throw new IllegalArgumentException("El artículo no debe ser nulo.");
		}
		if (articulo.getNombre() == null) {
			throw new IllegalArgumentException("El nombre del artículo no puede ser nulo.");
		}
		articulo.setNombre(articulo.getNombre().trim());
		if (articulo.getNombre().isEmpty()) {
			throw new IllegalArgumentException("El nombre del artículo no puede estar vacío.");
		}
		if (articulo.getPrecioUnidad() == null) {
			throw new IllegalArgumentException("El precio del artículo no puede ser nulo.");
		}
		if (articulo.getPrecioUnidad().compareTo(BigDecimal.ZERO) <= 0) {
			throw new IllegalArgumentException("El precio del artículo debe ser mayor a 0.");
		}
		articuloDao.create(articulo);
	}

	@Override
	public Articulo read(int id) {
		return articuloDao.read(id);
	}

	public void update(Articulo art) {
		if (art == null) {
			throw new IllegalArgumentException("El artículo no debe ser null");
		}
		if (!articuloDao.exists(art.getId())) {
			throw new IllegalArgumentException("El artículo no existe");
		}
		if (art.getPrecioUnidad() == null) {
			throw new IllegalArgumentException("El precio del artículo debe estar completo");
		}
		if (art.getPrecioUnidad().compareTo(BigDecimal.ZERO) <= 0) {
			throw new IllegalArgumentException("El precio debe ser mayor a 0");
		}
		if (art.getNombre() == null) {
			throw new IllegalArgumentException("El nombre no debe ser null");
		}
		art.setNombre(art.getNombre().trim());
		if (art.getNombre().isEmpty()) {
			throw new IllegalArgumentException("El nombre del artículo debe estar completo");
		}

		articuloDao.update(art);
	}

	public void remove(Articulo articulo) {
		/*
		 * if (articulo.getId() <= 0) { throw new IllegalArgumentException(
		 * "El artículo buscado no existe."); }
		 */
		articuloDao.delete(articulo);
	}

	@Override
	public List<Articulo> read() {
		return articuloDao.read();
	}

	public boolean exists(int id) {
		/*
		 * if (id <= 0) { return false; }
		 */
		return articuloDao.exists(id);
	}

}
