package edu.ltmj.bl.factory;

import edu.ltmj.bl.ArticuloBL;
import edu.ltmj.bl.ArticuloBLImpl;
import edu.ltmj.dao.ArticuloDao;
import edu.ltmj.dao.ArticuloDaoJdbcImpl;
import edu.ltmj.dao.ArticuloDaoMemoriaImpl;
import edu.ltmj.dao.ConeccionSql;

import java.util.HashMap;
import java.util.Map;

/**
 * Class that configures the beans used in the app.
 * Created by luiggimendoza on 5/14/16.
 */
public class ConfiguracionAplicacion {

    private Map<Class<?>, Object> mapaInstancias = new HashMap<>();

    public void init() {
        putInstance(ArticuloBLImpl.class, new ArticuloBLImpl());
        putInstance(ArticuloDaoJdbcImpl.class, new ArticuloDaoJdbcImpl());
        putInstance(ArticuloDaoMemoriaImpl.class, new ArticuloDaoMemoriaImpl());
        putInstance(ConeccionSql.class,new ConeccionSql());
    }

    public <T> T getInstance(Class<T> clazz) {
        return clazz.cast(mapaInstancias.get(clazz));
    }

    private <T> void putInstance(Class<T> clazz, T instance) {
        mapaInstancias.put(clazz, instance);
    }

    public void configuracionBaseDatos() {
        ArticuloDao articuloDao = getInstance(ArticuloDaoJdbcImpl.class);
        getInstance(ArticuloBLImpl.class).setArticuloDao(articuloDao);
        getInstance(ArticuloDaoJdbcImpl.class).setConeccionSql(getInstance(ConeccionSql.class));
        putInstance(ArticuloDao.class, articuloDao);
        putInstance(ArticuloBL.class, getInstance(ArticuloBLImpl.class));
    }

    public void configuracionMemoria() {
        ArticuloDao articuloDao = getInstance(ArticuloDaoJdbcImpl.class);
        getInstance(ArticuloBLImpl.class).setArticuloDao(getInstance(ArticuloDaoMemoriaImpl.class));
        putInstance(ArticuloDao.class, articuloDao);
        putInstance(ArticuloBL.class, getInstance(ArticuloBLImpl.class));
    }
}
