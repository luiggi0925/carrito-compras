package edu.ltmj.entidad;

import java.math.BigDecimal;

public class Articulo extends Entidad {

	private String nombre;
	private BigDecimal precioUnidad;

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public BigDecimal getPrecioUnidad() {
		return precioUnidad;
	}

	public void setPrecioUnidad(BigDecimal precioUnidad) {
		this.precioUnidad = precioUnidad;
	}
}
