CREATE TABLE articulo (
  id INT PRIMARY KEY AUTO_INCREMENT,
  nombre VARCHAR(100) NOT NULL,
  precioUnidad DECIMAL(7,2) NOT NULL
);

CREATE TABLE venta (
  id INT PRIMARY KEY AUTO_INCREMENT,
  fecha DATETIME NOT NULL,
  estado VARCHAR(10) NOT NULL,
  precioFinal DECIMAL(7,2)
);

CREATE TABLE articulo_x_venta (
  id INT PRIMARY KEY AUTO_INCREMENT,
  articuloId INT NOT NULL,
  ventaId INT NOT NULL,
  cantidadArticulos INT NOT NULL,
  precioTotal DECIMAL(7,2) NOT NULL
);

ALTER TABLE articulo_x_venta
 ADD FOREIGN KEY (articuloId) REFERENCES articulo(id);

ALTER TABLE articulo_x_venta
 ADD FOREIGN KEY (ventaId) REFERENCES venta(id);
